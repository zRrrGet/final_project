from typing import Generator, cast
from fastapi import FastAPI, Depends
from prometheus_client.exposition import CONTENT_TYPE_LATEST
from starlette.responses import JSONResponse, Response
import uvicorn
from pydantic import BaseModel, BaseSettings
import os
from psycopg2.extras import RealDictConnection, RealDictCursor
import logging
from prometheus_fastapi_instrumentator import Instrumentator


app = FastAPI()

Instrumentator(excluded_handlers=["/metrics"]).instrument(app).expose(app)


class RepoSettings(BaseSettings):
    user: str
    passwd: str
    host: str
    port: int
    db: str

    class Config:
        env_prefix = "REPO_"


class Note(BaseModel):
    text: str


class Notes(BaseModel):
    __root__: list[Note]


def conn() -> Generator[RealDictConnection, None, None]:
    config = RepoSettings()

    dsn = f"postgres://{config.user}:{config.passwd}@{config.host}:{config.port}/{config.db}"

    with RealDictConnection(dsn) as conn:
        with conn.cursor() as cursor:
            cursor.execute("create table if not exists note(text text)")
        conn.commit()
        yield conn


@app.post("/notes")
async def add_note(note: Note, conn: RealDictConnection = Depends(conn)):
    logging.error(conn)
    with conn.cursor() as cursor:
        cursor = cast(RealDictCursor, cursor)
        cursor.execute("INSERT INTO note (text) values(%s)", (note.text,))
        conn.commit()


@app.get("/notes", response_model=Notes)
async def get_notes(conn: RealDictConnection = Depends(conn)):
    with conn.cursor() as cursor:
        cursor = cast(RealDictCursor, cursor)
        cursor.execute("SELECT * FROM note")
        result = []
        for raw in cursor.fetchall():
            result.append(Note(text=raw["text"]))
        return result


@app.get("/health")
async def healthcheck():
    return JSONResponse({})
